package br.pucpr.bsi.projetoIntegrador.nomeProjeto.exception;


/**
 * Classe de Exception para o projeto do projeto integrador
 * @author Mauda
 *
 */

public class BSIException extends RuntimeException{

	private static final long serialVersionUID = 4928599035264976611L;
	
	public BSIException(String message) {
		super(message);
	}
	
	
	public BSIException(Throwable t) {
		super(t);
	}
}
