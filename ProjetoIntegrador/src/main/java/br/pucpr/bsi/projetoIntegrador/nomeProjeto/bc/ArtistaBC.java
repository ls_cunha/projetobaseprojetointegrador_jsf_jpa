package br.pucpr.bsi.projetoIntegrador.nomeProjeto.bc;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.pucpr.bsi.projetoIntegrador.nomeProjeto.dao.ArtistaDAO;
import br.pucpr.bsi.projetoIntegrador.nomeProjeto.exception.BSIException;
import br.pucpr.bsi.projetoIntegrador.nomeProjeto.model.Artista;

public class ArtistaBC extends PatternBC<Artista> {
	 
	private static ArtistaBC instance = new ArtistaBC();
	
	private ArtistaBC() {
	}

	public static ArtistaBC getInstance() {
		return instance;
	}

	@Override
	public long insert(Artista object) {
		validateForDataModification(object);
		return ArtistaDAO.getInstance().insert(object);
	}

	@Override
	public Artista findById(long id) {
		if(id < 0){
			return null;
		}
		return ArtistaDAO.getInstance().findById(id);
	}

	@Override
	public List<Artista> findAll() {
		return ArtistaDAO.getInstance().findAll();
	}

	@Override
	public List<Artista> findByFilter(Artista filter) {
		if(!validateForFindData(filter)){
			throw new BSIException("ER0001");
		}
		return ArtistaDAO.getInstance().findByFilter(filter);
	}
	
	@Override
	public boolean update(Artista object) {
		validateForDataModification(object);
		return ArtistaDAO.getInstance().update(object);
	}

	@Override
	public boolean delete(Artista object) {
		return ArtistaDAO.getInstance().delete(object);
	}

	@Override
	protected void validateForDataModification(Artista object) {
		if(object == null){
			throw new BSIException("ER0010");
		}
		
		if(StringUtils.isBlank(object.getNome())){
			throw new BSIException("ER0011");
		}
	}

	@Override
	protected boolean validateForFindData(Artista object) {
		return object != null && StringUtils.isNotBlank(object.getNome());
	}
}

