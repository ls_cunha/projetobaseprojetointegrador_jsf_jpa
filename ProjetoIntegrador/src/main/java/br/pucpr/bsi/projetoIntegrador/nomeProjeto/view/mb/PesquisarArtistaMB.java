package br.pucpr.bsi.projetoIntegrador.nomeProjeto.view.mb;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import br.pucpr.bsi.projetoIntegrador.nomeProjeto.bc.ArtistaBC;
import br.pucpr.bsi.projetoIntegrador.nomeProjeto.exception.BSIException;
import br.pucpr.bsi.projetoIntegrador.nomeProjeto.model.Artista;
import br.pucpr.bsi.projetoIntegrador.nomeProjeto.view.mb.utils.ViewUtil;
import br.pucpr.bsi.projetoIntegrador.nomeProjeto.view.messages.MessagesUtils;

/**
 * @author Everson Mauda
 * @version 1.0.0
 */

@ManagedBean
@ViewScoped
public class PesquisarArtistaMB implements Serializable{
	
	/////////////////////////////////////
	// Atributos
	/////////////////////////////////////
	
	private static final long serialVersionUID = 1L;
	
	public static final String ARTISTA_SELECIONADA = "artistaSelecionada";
	public static final String FILTRO_PESQUISA = "filtroPesquisa";
	
	//Utilizado para logs via Log4J
	private static Logger log = Logger.getLogger(PesquisarArtistaMB.class);
	
	private Artista filtroPesquisa;
	
	private List<Artista> artistas;
	private Artista artistaSelecionada;
	
	/////////////////////////////////////
	// Construtores
	/////////////////////////////////////
	
	public PesquisarArtistaMB() {
	}
	
	@PostConstruct
	private void init(){
		filtroPesquisa = (Artista) ViewUtil.getParameter(FILTRO_PESQUISA);
		log.debug("Valor do Filtro:" + filtroPesquisa);
		if(filtroPesquisa == null){
			filtroPesquisa = new Artista();
		} else {
			pesquisar();
		}
	}
	
	/////////////////////////////////////
	// Actions
	/////////////////////////////////////
	
	//Action de Pesquisar a partir do filtro
	public void pesquisar() {
		artistas = ArtistaBC.getInstance().findByFilter(filtroPesquisa);
		if(artistas.isEmpty()){
			MessagesUtils.addInfo("informacao", "IN0001");
		}
	}
	
	public String editar(){
		log.debug("Iniciando a operacao de editar");
		validate();
		setParameters(ManterArtistaMB.Acoes.EDITAR);
		return "manterArtista";
	}
	
	public String excluir(){
		log.debug("Iniciando a operacao de excluir");
		validate();
		setParameters(ManterArtistaMB.Acoes.EXCLUIR);
		return "manterArtista";
	}
	
	public String visualizar(){
		log.debug("Iniciando a operacao de visualizar");
		validate();
		setParameters(ManterArtistaMB.Acoes.VISUALIZAR);
		return "manterArtista";
	}
	
	/////////////////////////////////////
	// Metodos Utilitarios
	/////////////////////////////////////
	
	private void validate(){
		if(artistaSelecionada == null){
			throw new BSIException("ER0052");
		}
	}
	
	private void setParameters(ManterArtistaMB.Acoes acao){
		ViewUtil.setRequestParameter(ARTISTA_SELECIONADA, artistaSelecionada);
		ViewUtil.setRequestParameter(FILTRO_PESQUISA, filtroPesquisa);
		ViewUtil.setRequestParameter(acao);
	}
	
	/////////////////////////////////////
	// Getters and Setters
	/////////////////////////////////////
	
	public Artista getFiltroPesquisa() {
		return filtroPesquisa;
	}
	
	public List<Artista> getArtistas() {
		return artistas;
	}
	
	public Artista getArtistaSelecionada() {
		return artistaSelecionada;
	}
	
	public void setArtistaSelecionada(Artista selecionada) {
		artistaSelecionada = selecionada;
	}
}